//Requiring the package
var PouchDB = require("PouchDB");
var db = new PouchDB("todo-db");
var remote = 'http://merjed:merjed@localhost:5984/todo-db';
var opts = { live: true, retry: true };
db.sync(remote,opts).on('change', function (info) {
    // handle change
    console.log('Changement sur la BD');
  }).on('paused', function (err) {
    // replication paused (e.g. replication up to date, user went offline)
    console.log('replication paused');
  }).on('active', function () {
    // replicate resumed (e.g. new changes replicating, user went back online)
    console.log('replicate resumed');
  }).on('denied', function (err) {
    // a document failed to replicate (e.g. due to permissions)
    console.log('document failed to replicate');
  }).on('complete', function (info) {
    // handle complete
    console.log('handle complete');
  }).on('error', function (err) {
    // handle error
    console.log('handle error '+err);
  });
module.exports.db = db;
