const express = require("express");
const app = express();
const cors = require("cors");
const db = require("./db.js");
const todoRoute = require("./routers/todo.js");
const tagRoute = require("./routers/tag.js");

//Middleware
app.use(cors());
app.use(express.json());

app.use("/todos", todoRoute);
app.use("/tags", tagRoute);

app.get("/", (req, res) => {
  res.send("Welcome TODO (pouchdb)");
});

const PORT = 3001;
app.listen(PORT, (port) => {
  port = PORT;
  console.log(`Listening on port : ${port}`);
});
