const express = require("express");
const router = express.Router();
const dbCnx = require("../db.js");
const db = dbCnx.db;
let todoCount = 0;
let todosList = [];

const NodeCouchDb = require('node-couchdb');
const couch = new NodeCouchDb({
    auth: {
        user: 'merjed',
        pass: 'merjed'
    }
});

router.get("/", (request, response) => {
  couch.get('todo-db', '_design/todos/_view/todos-list', {}).then(
      function(result){
        todosCount = result.data.total_rows;
        response.send(`<h1>Total todos: ${todosCount}</h1>`);
      }
  ).catch(function (err) {
      console.log(err);
    });
});

router.get("/list", (request, response) => {
  couch.get('todo-db', '_design/todos/_view/todos-list', {}).then(
    function(result){
      todosList = result.data.rows;
     response.send(todosList);
    }
).catch(function (err) {
    console.log(err);
  });
});

router.get("/list/:tag", (request, response) => {
  let tagsArray=[]
  let tagsList=[]
  const tagId=request.params.tag;
    couch.get('todo-db', '_design/todos/_view/todos-list', {}).then(
        function(result){
          tagsArray = result.data.rows;
          tagsArray.map(row => 
            {
              console.log(row)
              if(row.value.tag==tagId && row.value.visible==undefined)
                tagsList.push(row.value) 
            })
         response.send(tagsList);
        }
    ).catch(function (err) {
        console.log(err);
      });
});

router.post("/new/:tag", (request, response) => {
  const title = request.body.title;
  const tag = request.params.tag;
  const newtodo = {
    title: title,
    complete: false,
    doc_type: 'todo',
    tag : tag
  };
  db.post(newtodo, function (err, res) {
    if (err) {
      return console.log(err);
    }
    response.send(newtodo);
  });
  
});

router.put("/update/:id", (request, response) => {
  const id = request.params.id;
  const title = request.body.title;
  db.get(id, (err, todo) => {
    const newTodo = {
      _id: todo._id,
      _rev: todo._rev,
      title: title,
      complete: todo.complete,
      doc_type: todo.doc_type,
      tag : todo.tag
    };
    db.put(newTodo, (err, res) => {
      if (err) {
        console.log(err);
        response.send("ERREUR");
      } else response.send(res);
    });
  });
});

router.delete("/delete/:id", (request, response)=>{
  const id = request.params.id;
  db.get(id, (err,todo) => {
    if(err)
    response.send('erreur '+err)
    else
    {
      db.remove(todo, (error, result) =>{
        if(error)
        response.send('Erreur suppression')
        else
        response.send('Todo supprimée')
      });
    }
  })
})

router.put('/actions/:id/', (request, response) => { 
  const id = request.params.id;
  const action = request.body.complete;
  let complete = (action === true) ? true : false;
  db.get(id, (err, todo) => {
    let newTodo = {
      _id: todo._id,
      _rev: todo._rev,
      title: todo.title,
      complete : complete,
      doc_type: todo.doc_type,
      tag: todo.tag
    }
    db.put(newTodo, (err, res) => {
      if (err) {
        console.log(err);
        response.send("ERREUR changement complete");
      } 
      else 
        response.send(newTodo);
    });
  })
});

router.put('/actions/multiple/:action', (request, response) => {  
  const action = request.params.action;
  let complete=false;
  if(action=="true")
    complete=true;
  let newTodos=[];
  console.log(action)
  db.allDocs({ include_docs: true })
  .then(function (result) {
    let  todosList = result.rows;
    todosList.map(todo => {
      console.log(todo.doc._id, todo.doc._rev);
      const t = {
        _id: todo.doc._id,
        _rev: todo.doc._rev,
        title:todo.doc.title,
        complete:complete
      }
      newTodos.push(t);
    })
    db.bulkDocs(newTodos).then(function (result) {
      // handle result
      response.send('Todos modifiés en '+complete)
    }).catch(function (err) {
      console.log(err);
      response.send('Erreur modification todos '+err)
    });
  })
  .catch(function (err) {
    console.log(err);
  });
});

module.exports = router;
