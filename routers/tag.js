const express = require("express");
const router = express.Router();
const dbCnx = require("../db.js");
const db = dbCnx.db;
let tagCount = 0;
let tagsList = [];
const NodeCouchDb = require('node-couchdb');
const couch = new NodeCouchDb({
    auth: {
        user: 'merjed',
        pass: 'merjed'
    }
});

router.get("/", (request, response) => {
    
    couch.get('todo-db', '_design/tags/_view/tags-list', {}).then(
        function(result){
          tagsCount = result.data.total_rows;
          response.send(`<h1>Total todos: ${tagsCount}</h1>`);
        }
    ).catch(function (err) {
        console.log(err);
      });
});

router.get("/list", (request, response) => {
  let tagsArray=[]
  let tagsList=[]
    couch.get('todo-db', '_design/tags/_view/tags-list', {}).then(
        function(result){
          tagsArray = result.data.rows;
          tagsArray.map(row => tagsList.push(row.value) )
        //  response.send(tagsList);
         couch.get('todo-db', `_design/tags/_view/count-todo-in-tag?group_level=1`, {}).then(
          function(result){
            let tags=[...tagsList];
            const todoByTag=result.data.rows;
            todoByTag.map(t => {
              const tag=tags.find(tg => t.key===tg._id)
              t._id=tag._id;
              t._rev=tag._rev;
              t.title=tag.title;
              t.doc_type=tag.doc_type;
              t.bg_color=tag.bg_color;
            })
            response.send(todoByTag);
          }
      ).catch(function (err) {
          console.log(err);
        });
        }
    ).catch(function (err) {
        console.log(err);
      });
      
});

router.get("/counttTodo/", (request, response) => {
    let llist=[];
    couch.get('todo-db', `_design/tags/_view/count-todo-in-tag?group_level=1`, {}).then(
        function(result){
          const todoByTag=result.data.rows;
          response.send(todoByTag);
        }
    ).catch(function (err) {
        console.log(err);
      });
      
});

// router.get("/countTodo/:tag", (request, response) => {
//   let tagsArray=[]
//   let countTodos=0
//   const tag=request.params.tag;
//     couch.get('todo-db', `_design/todos/_view/todos-list`, {}).then(
//         function(result){
//           tagsArray = result.data.rows;
//           tagsArray.map(row => 
//           {
//             if(row.value.tag && row.value.tag==tag && row.value.complete==false)
//             countTodos=countTodos+1;
//           })
//          response.send({count:countTodos });
//         }
//     ).catch(function (err) {
//         console.log(err);
//       });
// });

router.post("/new", (request, response) => {
  const title=request.body.title;
  const newTag = {
    title: title,
    doc_type: 'tag'
    };
    db.post(newTag, function (err, res) {
    if (err) {
        return console.log(err);
    }
    response.send(newTag);
    });
});

router.put("/update/:id", (request, response) => {
    const id = request.params.id;
    const title = request.body.title;
    db.get(id, (err, tag) => {
      const newTag = {
        _id: tag._id,
        _rev: tag._rev,
        title: title,
        doc_type : tag.doc_type
      };
      db.put(newTag, (err, res) => {
        if (err) {
          console.log(err);
          response.send("ERREUR");
        } else response.send(res);
      });
    });
  });

  router.delete("/delete/:id", (request, response)=>{
    const id = request.params.id;
    db.get(id, (err,tag) => {
      if(err)
      response.send('erreur '+err)
      else
      {
        db.remove(tag, (error, result) =>{
          if(error)
          response.send('Erreur suppression')
          else
          response.send('Tag supprimée')
        });
      }
    })
  });

  module.exports = router;